import json
import numpy as np
import math
import logging
from collections import namedtuple

# logging.basicConfig(filename="agar_strategy.log", filemode="w", level=logging.DEBUG, format="%(message)s")


class Const:
    FOOD_RADIUS = 2.5
    FOOD_MASS = 1.0
    GAME_HEIGHT = 660
    GAME_WIDTH = 660
    VIRUS_RADIUS = 15
    MASS_EAT_FACTOR = 1.20
    MIN_SPLIT_MASS = 120.0
    VIS_FACTOR = 4.0
    VIS_SHIFT = 10
    VIS_FACTOR_FR = 2.5

    @classmethod
    def apply(cls, config):
        cls.FOOD_MASS = config.get("FOOD_MASS")
        cls.GAME_HEIGHT = config.get("GAME_HEIGHT")
        cls.GAME_WIDTH = config.get("GAME_WIDTH")
        cls.VIRUS_RADIUS = config.get("VIRUS_RADIUS")


Circle = namedtuple("Circle", ["pos", "r"])


class Unit:
    def __init__(self, x, y, r, m, uid=None, sx=0, sy=0, last_time=0):
        self.uid = uid
        self.pos = np.array([x, y])
        self.r = r
        self.m = m
        self.s = np.array([sx, sy])
        self.last_time = last_time

    def __hash__(self) -> int:
        return self.uid.__hash__()

    def __eq__(self, other):
        return isinstance(other, Unit) and self.uid == other.uid

    @property
    def speed(self):
        return np.linalg.norm(self.s)

    @property
    def visible_area(self):
        pos = self.pos
        if self.speed > 0:
            pos += self.s / self.speed * Const.VIS_SHIFT
        return Circle(pos=pos, r=self.visibility_radius)

    @property
    def visibility_radius(self):
        return Const.VIS_FACTOR * self.r

    def __repr__(self):
        return "Unit {}".format(self.uid)

    @staticmethod
    def make_food(obj):
        return Unit(obj.get('X'), obj.get('Y'), Const.FOOD_RADIUS, Const.FOOD_MASS)

    @staticmethod
    def make_player(obj):
        return Unit(obj.get('X'), obj.get('Y'), obj.get('R'), obj.get('M'),
                    obj.get("Id"))

    @staticmethod
    def make_mine(obj):
        return Unit(obj.get('X'), obj.get('Y'), obj.get('R'), obj.get('M'),
                    obj.get("Id"), obj.get("SX"), obj.get("SY"))


def unpack(data):
    objects = data.get("Objects")
    foods = list(map(Unit.make_food, filter(lambda o: o.get('T') == 'F', objects)))
    enemies = sorted(list(map(Unit.make_player, filter(lambda o: o.get('T') == 'P', objects))), key=lambda e: e.m)
    viruses = list(map(Unit.make_player, filter(lambda o: o.get('T') == 'V', objects)))
    mine = sorted(list(map(Unit.make_mine, data.get("Mine"))), key=lambda m: m.m)
    return mine, foods, enemies, viruses


def distance(p1, p2):
    return np.sqrt(np.sum((p2 - p1) ** 2))


def distance_to_set(p1, p2):
    return min(map(lambda p: distance(p1, p), p2))


class Exploration:
    def __init__(self):
        self._xy = [(Const.GAME_WIDTH * 0.2, Const.GAME_HEIGHT * 0.8),
               (Const.GAME_WIDTH * 0.8, Const.GAME_HEIGHT * 0.8),
               (Const.GAME_WIDTH * 0.8, Const.GAME_HEIGHT * 0.2),
               (Const.GAME_WIDTH * 0.2, Const.GAME_HEIGHT * 0.2),
               (Const.GAME_WIDTH * 0.3, Const.GAME_HEIGHT * 0.3),
               (Const.GAME_WIDTH * 0.7, Const.GAME_HEIGHT * 0.3),
               (Const.GAME_WIDTH * 0.7, Const.GAME_HEIGHT * 0.7),
               (Const.GAME_WIDTH * 0.3, Const.GAME_HEIGHT * 0.7)]
        self._next_xy = None

    def xy(self, me):
        if self._next_xy is None:
            self._next_xy = min(self._xy, key=lambda p: distance(p, me.pos))
        if distance(me.pos, np.array(self._next_xy)) < me.r:
            idx = (self._xy.index(self._next_xy) + 1) % len(self._xy)
            self._next_xy = self._xy[idx]
        return self._next_xy


def reachable(r, unit):
    left = 0 <= unit.pos[0] <= r
    right = Const.GAME_WIDTH - r <= unit.pos[0] <= Const.GAME_WIDTH
    top = 0 <= unit.pos[1] <= r
    down = Const.GAME_HEIGHT - r <= unit.pos[1] <= Const.GAME_HEIGHT
    return not ((left and top or left and down or right and top or right and down) and
                not np.sum((unit.pos - r) ** 2) <= r * r)


def can_be_eaten(victim_m, hunter_m):
    return hunter_m >= Const.MASS_EAT_FACTOR * victim_m


def find_victim(me, enemies):
    victim = None
    for e in filter(lambda e: can_be_eaten(e.m, me.m), enemies):
        if victim is None or distance(me.pos, e.pos) < distance(me.pos, victim.pos):
            victim = e
    return victim


def min_mass_after_splitting(units):
    big = list(filter(lambda u: u.m > Const.MIN_SPLIT_MASS, units))
    return min(map(lambda u: u.m / 2, big)) if big else 0


def find_closest_hunter(me, enemies):
    hunter = None
    for e in filter(lambda e: can_be_eaten(me.m, e.m), enemies):
        if hunter is None or distance(me.pos, e.pos) < distance(me.pos, hunter.pos):
            hunter = e
    return hunter


def find_hunters(me, enemies):
    return list(filter(lambda e: can_be_eaten(me.m, e.m) and visible([e, ], me), enemies))


def inside(p, eps=0.01):
    return -eps <= p[0] <= Const.GAME_WIDTH + eps and \
           -eps <= p[1] <= Const.GAME_HEIGHT + eps


def force_to_be_inside(p, pad=0):
    p[0] = min(max(pad, p[0]), Const.GAME_WIDTH - pad)
    p[1] = min(max(pad, p[1]), Const.GAME_HEIGHT - pad)
    return p


def meeting_point(hunter, victim):
    eps = 0.1

    def f(t):
        if hunter.speed <= eps:
            return 0
        return min(0, t - distance(victim.pos + victim.s * t, hunter.pos) / hunter.speed)

    t1 = 0
    t2 = 600
    if f(0) >= f(1):
        return victim.pos

    while t2 - t1 > 2:
        h = (t2 - t1) // 3
        t11 = t1 + h
        t21 = t2 - h
        if f(t11) < f(t21):
            t1 = t11
        else:
            t2 = t21

    t = (t1 + t2) // 2

    # logging.debug("f(t1) == {}, f(t2) == {}, f(t) == {}".format(f(0), f(1), f(t)))
    # logging.debug("{} {} {} {}".format(max(range(200), key=f), t, victim.s, victim.speed))

    # logging.debug("1 {} 2 {}".format(victim.pos, force_to_be_inside(victim.pos + victim.s * t, pad=victim.r)))
    return force_to_be_inside(victim.pos + victim.s * t, pad=victim.r)


def angle(v):
    alpha = math.acos(v[0] / np.linalg.norm(v))
    return alpha if v[1] >= 0 else 2 * math.pi - alpha


def find_escape(me, hunters, esc_radius):
    me_r = me.r
    me = me.pos
    hunters = list(map(lambda h: h.pos, hunters))

    def intersection(x, x0, y0, r, x_is_y=False):
        s = r ** 2 - (x - x0) ** 2
        if s <= 0:
            return []
        s = np.sqrt(s)
        if x_is_y:
            return [np.array([y0 + s, x]), np.array([y0 - s, x])]
        return [np.array([x, y0 + s]), np.array([x, y0 - s])]

    def rotated(v, alpha):
        rot = np.array([[np.cos(alpha), -np.sin(alpha)],
                        [np.sin(alpha), np.cos(alpha)]])
        return rot.dot(v)

    corners = [np.array([0, 0]), np.array([Const.GAME_WIDTH, 0]),
               np.array([0, Const.GAME_HEIGHT]), np.array([Const.GAME_WIDTH, Const.GAME_HEIGHT])]

    borders = []
    borders.extend(intersection(0, me[0], me[1], esc_radius))
    borders.extend(intersection(0, me[1], me[0], esc_radius, x_is_y=True))
    borders.extend(intersection(Const.GAME_WIDTH, me[0], me[1], esc_radius))
    borders.extend(intersection(Const.GAME_HEIGHT, me[1], me[0], esc_radius, x_is_y=True))
    borders = list(filter(lambda p: len(p) == 2 and inside(p, eps=0.01), borders))

    danger_dir = [h - me for h in hunters] + list(map(lambda p: p - me, corners))
    danger_dir.sort(key=angle)
    danger_dir = list(map(lambda d: d / np.linalg.norm(d) * esc_radius, danger_dir))

    escapes = borders
    escapes.extend([me + esc_radius * rotated([1, 0], a) for a in np.linspace(0, 2 * np.pi, 24)])
    for i in range(len(danger_dir)):
        escapes.append(me + 0.5 * (danger_dir[i - 1] + danger_dir[i]))
    for dir in danger_dir[:-4]:
        escapes.append(me - dir)

    escapes = list(map(lambda e: force_to_be_inside(e, me_r), escapes))

    to_avoid = hunters + corners

    return max(escapes, key=lambda p: distance_to_set(p, to_avoid))


def visible(squad, unit):
    for c in map(lambda m: m.visible_area, squad):
        if distance(c.pos, unit.pos) < c.r + unit.r:
            return True
    return False


class Strategy:
    ENEMY_MEM_TIME = 100

    def __init__(self):
        self.time = 0
        self.enemies = []
        self.exp = None

    def run(self):
        Const.apply(json.loads(input()))
        self.exp = Exploration()
        while True:
            self.time += 1
            data = json.loads(input())
            cmd = self.on_tick(data)
            print(json.dumps(cmd), flush=True)

    def update_enemies(self, mine, enemies):
        enemies_dict = dict()
        # logging.debug("1")
        for e in filter(lambda e: e.last_time + self.ENEMY_MEM_TIME > self.time
                and (not visible(mine, e) or e in enemies), self.enemies):
            enemies_dict[e.uid] = e
        # logging.debug("2")
        for e in enemies:
            e.last_time = self.time
            oldE = enemies_dict.get(e.uid)
            enemies_dict[e.uid] = e
            if oldE is not None:
                enemies_dict[e.uid].s = e.pos - oldE.pos
                # logging.debug("{} {} {}".format(e.pos, oldE.pos, enemies_dict[e.uid].speed))
        # logging.debug("3")
        self.enemies = list(enemies_dict.values())
        # logging.debug("4")
        return sorted(self.enemies, key=lambda e: e.m)

    def on_tick(self, data):
        mine, foods, enemies, viruses = unpack(data)
        enemies = self.update_enemies(mine, enemies)
        # logging.debug("enemies %s", enemies)

        if mine:
            for me in mine[::-1]:
                hunters = find_hunters(me, enemies)
                if hunters:
                    escape = me.pos + sum(map(lambda m: m.r / me.r, mine)) * (find_escape(me, hunters, 2 * me.r) - me.pos)
                    return self._act(escape, debug="hunter")

            for me in mine:
                victim = find_victim(me, enemies)
                if victim:
                    aim = meeting_point(me, victim)
                    angle_diff = abs(angle(me.s) - angle(aim - me.pos))
                    split = can_be_eaten(victim.m, me.m / 2) and \
                        not can_be_eaten(min_mass_after_splitting(mine), enemies[-1].m) and \
                        min(angle_diff, 2 * math.pi - angle_diff) <= 0.167 * math.pi
                    return self._act(aim, split=split, debug="victim")

            me = mine[0]
            foods = list(filter(lambda f: reachable(me.r, f) and
                                not visible(filter(lambda e: can_be_eaten(me.m, e.m), enemies), f), foods))
            if foods:
                food_pos = min(map(lambda f: f.pos, foods), key=lambda p: distance(p, me.pos))
                food_pos = me.pos + len(mine) * (food_pos - me.pos)
                return self._act(food_pos, debug="food")
            else:
                return self._act(self.exp.xy(me), debug="explore")
        return self._act((0, 0), debug="died")

    def _act(self, aim=(0, 0), split=False, eject=False, debug=""):
        debug = "Tick {}: {}: {}".format(self.time, debug, np.array(aim))
        # logging.debug(debug)
        o = {'X': float(aim[0]), 'Y': float(aim[1])}
        if split: o["Split"] = True
        if eject: o["Eject"] = True
        if debug: o["Debug"] = debug
        return o


if __name__ == '__main__':
    Strategy().run()